const mongoose = require('mongoose')
const marked = require('marked') //convert markdown to html
const slugify = require('slugify') //change the url as specified
const createDomPurify = require('dompurify')
const { JSDOM } = require('jsdom') // sanitize html
const domPurify = createDomPurify(new JSDOM().window)

const articleSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    markdown: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    slug: {
        type: String,
        unique: true,
        required: true
    },
    // sanitizedHtml: {
    //     type: String,
    //     required: true
    // }
})

articleSchema.pre('validate', function (next) {
    if (this.title) {
        this.slug = slugify(this.title, { lower: true, strict: true })
    }

    // if (this.markdown) {
    //     this.sanitizedHtml = domPurify.sanitize(marked(this.markdown))
        
    // }
    next()
})


const ArticleModel = mongoose.model('Article', articleSchema)

module.exports = ArticleModel