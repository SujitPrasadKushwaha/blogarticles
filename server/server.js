const express = require('express')
const mongoose = require('mongoose')
const articleRouter = require('./routes/articles')
const app = express()
const bodyParse = require('body-parser')
const ArticleModel = require('./models/articles')
const methodOverride = require('method-override')


mongoose.connect('mongodb://localhost/blog', { useNewUrlParser: true, useUnifiedTopology: true })



app.set('view engine', 'ejs')
app.use(bodyParse.urlencoded({ extended: true }))
app.use(methodOverride('_method'))


app.get('/', async (req, res) => {
    const articles = await ArticleModel.find().sort({
        createdAt: 'desc'
    })
    res.render('articles/index', { articles: articles })
})

app.use('/articles', articleRouter)


app.listen(8000);